import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// https://back-elets.admlr.lipetsk.ru/news/all/
//https://back-elets.admlr.lipetsk.ru/news/15/

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
